import 'package:flutter/material.dart';

class IntroPage2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Androidmonks",
      home: Scaffold(
        body: Stack(
          children: <Widget>[
            PageView(
              children: <Widget>[
                Container(
                  child: Column(
                    children: <Widget>[
                      Image.network(
                          "https://image.freepik.com/free-vector/children-background_23-2147508930.jpg"),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        "Hay chung tay de bao ve tre em",
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Text(
                        "Suc khoe tre em duoc dat len hang dau ",
                        style: TextStyle(fontSize: 20, color: Colors.black),
                      ),
                      SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(
                            width: 10,
                            height: 120,
                          ),
                          Text("SKIP",
                              style: TextStyle(
                                fontSize: 20,
                              )),
                          SizedBox(
                            width: 290,
                          ),
                          Text("Next",
                              style: TextStyle(
                                fontSize: 20,
                              ))
                        ],
                      )
                    ],
                  ),
                ),
                Container(
                  child: Center(child: Text("Page 2")),
                  color: Colors.blueAccent,
                )
              ],
              pageSnapping: false,
            ),
          ],
        ),
      ),
    );
  }
}
