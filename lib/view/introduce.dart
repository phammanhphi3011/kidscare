import 'package:flutter/material.dart';
//import 'package:kidscare/view/introduce1.dart';

class IntroPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: Column(
          children: <Widget>[
            Image.network(
                "https://image.freepik.com/free-vector/children-background_23-2147508930.jpg"),
            SizedBox(
              height: 20,
            ),
            Text(
              "Hay chung tay de bao ve tre em",
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              "Suc khoe tre em duoc dat len hang dau ",
              style: TextStyle(fontSize: 20, color: Colors.black),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 10,
                  height: 175,
                ),
                Text("SKIP",
                    style: TextStyle(
                      fontSize: 20,
                    )),
                SizedBox(
                  width: 300,
                ),
                Text("Next",
                    style: TextStyle(
                      fontSize: 20,
                    ))
              ],
            )
          ],
        ),
      ),
    ));
  }
}
