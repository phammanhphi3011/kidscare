import 'package:flutter/material.dart';
import 'package:kidscare/view/introduce.dart';
import 'package:kidscare/view/BodyChart.dart';
import 'package:kidscare/view/introduce2.dart';

//import 'package:kidscare/view/introduce1.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: HomeScreen());
  }
}

class HomeScreen extends StatelessWidget {
  //List<Widget> myPages = [IntroPage(), IntroPage1()];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('my frist home'),
      ),
      //   body: PageView(
      //  children: <Widget>[IntroPage(), IntroPage1()],
      //   ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          backgroundColor: Colors.orange,
          onPressed: () {
            print('cliked');
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => DetailsPage()));
          }),
    );
  }
}
